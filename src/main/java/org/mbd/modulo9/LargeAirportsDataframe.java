package org.mbd.modulo9;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class LargeAirportsDataframe {
    public static void main(String[] args) {

        Logger.getLogger("org").setLevel(Level.OFF);

        SparkSession sparkSession = SparkSession
                .builder()
                .appName("Airport data processing with Spark dataframes")
                .master("local[2]")
                //.config("spark.some.config.option", "some-value")
                .getOrCreate();

        Dataset<Row> airport_dataframe = sparkSession
                .read()
                .format("csv")
                .option("header", "true")
                .option("inferSchema", "true")
                .load("data/airports.csv")
                .cache();

        //airport_dataframe.printSchema();
        //airport_dataframe.show();


        Dataset<Row> large_airports = airport_dataframe
                .filter(airport_dataframe.col("type").equalTo("large_airport"))
                .groupBy("iso_country").count();

        //large_airports.printSchema();
        //large_airports.show();

        Dataset<Row> countries = sparkSession
                .read()
                .format("csv")
                .option("header", "true")
                .option("inferSchema", "true")
                .load("data/countries.csv")
                .cache();

        //countries.printSchema();
        //countries.show();

        //Join the dataframes
        Dataset<Row> join = large_airports
                .join(countries, large_airports.col("iso_country")
                        .equalTo(countries.col("code")),"full_outer")
                .select("name", "count")
                .sort(col("count").desc());

        System.out.println("List of the countries having the largest number of large airports.");
        join.printSchema();
        join.show();


    }
}
