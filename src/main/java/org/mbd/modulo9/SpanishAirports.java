package org.mbd.modulo9;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.PrintWriter;
import java.util.List;

public class SpanishAirports {
    public static void main(String[] args) {
        Logger.getLogger("org").setLevel(Level.OFF);

        // STEP 2: create a SparkConf object
        SparkConf sparkConf = new SparkConf()
                .setAppName("Accumulators")
                .setMaster("local[2]");

        // STEP 3: create a Java Spark context
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        // STEP 4: read lines of files
        JavaRDD<String> lines = sparkContext.textFile("data/airports.csv");

        JavaRDD<String[]> fields = lines.map(line -> line.split(","));

        JavaRDD<String[]> spanish_airport = fields
                .filter(x -> x[8].equals("\"ES\""));

        JavaPairRDD<String, Integer> type_airport = spanish_airport
                .mapToPair(x -> new Tuple2<String, Integer>(x[2], 1));

        // STEP: sum the numbers
        JavaPairRDD<String, Integer> cuentas = type_airport.reduceByKey((x1, x2) -> x1 + x2);

        List<Tuple2<String, Integer>> output = cuentas.
                sortByKey(false).collect();

        // STEP 9: print the results in console
        for (Tuple2<?, ?> tuple : output) {
            System.out.println(tuple._1() + ": " + tuple._2());
        }
        // STEP 10: print the results in text file
        try {
            PrintWriter aeropuertos = new PrintWriter("aeropuertos.txt");
            for (Tuple2<?, ?> tuple : output) {
                aeropuertos.println(tuple._1() + ": " + tuple._2());
            }
            aeropuertos.close();

        }
        catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("No existe el archivo.");
        }

        // STEP 19: stop the spark context
        sparkContext.stop();


    }
}
