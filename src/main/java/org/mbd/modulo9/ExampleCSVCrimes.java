package org.mbd.modulo9;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class ExampleCSVCrimes {
    public static void main(String[] args) {

        Logger.getLogger("org").setLevel(Level.OFF);

        SparkSession sparkSession = SparkSession
                .builder()
                .appName("Java Spark SQL. Example of reading a CSV file")
                .master("local[2]")
                //.config("spark.some.config.option", "some-value")
                .getOrCreate();

        Dataset<Row> dataFrame = sparkSession
                .read()
                .format("csv")
                .option("header", "true")
                .option("inferSchema", "true")
                .load("data/PDR2018.csv")
                .cache();

        //dataFrame.printSchema();
        //dataFrame.show();

        Dataset<Row> categories = dataFrame
                .groupBy("Incident Category")
                .count()
                .sort(col("count").desc()) ;

        categories.printSchema() ;
        categories.show();

        long numberOfCrimesInDistrict = dataFrame
                .filter(col("Police District").equalTo("Richmond"))
                .count();

        System.out.println("Number of incidences in a district Richmond: " + numberOfCrimesInDistrict);

    }
}