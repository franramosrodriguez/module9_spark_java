package org.mbd.modulo9;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Program that generetes random numbers in a map operation
 *
 * @author Antonio J. Nebro
 */
public class RandomNumbersV2 {
    public static void main(String[] args) {
        Logger.getLogger("org").setLevel(Level.OFF);

        SparkConf sparkConf = new SparkConf()
                .setAppName("Random numbers")
                .setMaster("local[2]");

        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i < 32; i++) {
            integerList.add(i);
        }

        Random rnd = new Random();

        List<Integer> result = sparkContext
                .parallelize(integerList)
                .map(value -> {
                    rnd.setSeed(value);
                    return rnd.nextInt(100) ;
                })
                .collect();

        for (Integer value : result) {
            System.out.print(value + " ");
        }

        /*
        List<Integer> sortedList = result.stream().sorted().collect(Collectors.toList());

        System.out.println() ;
        for (Integer value : sortedList) {
          System.out.print(value + " ") ;
        }
        */

        sparkContext.stop();
    }
}
